import 'package:json_annotation/json_annotation.dart';

part 'chapter.g.dart';

@JsonSerializable()
class Chapter {
  int id; //
  String name; //
  @JsonKey(name: "description")
  String testOn; //

  Chapter();

  factory Chapter.fromJson(Map<String, dynamic> json) => _$ChapterFromJson(json); //
  Map<String, dynamic> toJson() => _$ChapterToJson(this);

  bool get invalid => (name?.isEmpty ?? true) || (testOn?.isEmpty ?? true); //
  List<String> get skills => testOn?.split(";")?.map((e) => e.trim())?.toList(); //

  String urlSkills(int school) => "concepts/skill/$id/school/$school";
}
