import 'package:json_annotation/json_annotation.dart';

part 'skill.g.dart';

@JsonSerializable()
class Skill {
  int id; //
  @JsonKey(name: 'description')
  String name; //

  Skill();

  factory Skill.fromJson(Map<String, dynamic> json) => _$SkillFromJson(json); //
  Map<String, dynamic> toJson() => _$SkillToJson(this);
}
