import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:genius_corner/create_quiz/in_create.dart';
import 'package:genius_corner/create_quiz/list_sections.dart';
import 'package:genius_corner/gui/check_option.dart';
import 'package:genius_corner/gui/input_label.dart';
import 'package:genius_corner/gui/numeric_input.dart';
import 'package:genius_corner/gui/submit_button.dart';
import 'package:genius_corner/gui/text_input.dart';
import 'package:genius_corner/gui/time_picker.dart';
import 'package:genius_corner/json/quiz.dart';
import 'package:genius_corner/misc/form_option.dart';
import 'package:intl/intl.dart';

class TabDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    InCreate settings = InCreate.of(context);

    return FormBuilder(
      initialValue: {
        "teacherId": settings.userId,
        "assessmentType": "Objective",
      },
      child: Builder(
        builder: (context) {
          return ListView(
            shrinkWrap: true,
            children: [
              _Island(
                children: [
                  _Required(true),
                  TextInput(
                    attribute: "description",
                    label: "Assessment Name",
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      NumericInput(
                          attribute: "totalQuestions", min: 1, label: "Total Questions"),
                      SizedBox(width: 20),
                      NumericInput(
                        attribute: "totalMarks",
                        min: 1,
                        label: "Total Marks",
                      ),
                    ],
                  ),
                  InputLabel("Select class for which you want to make this assessment"),
                  ListSections(),
                  InputLabel("Assessment Start Date"),
                  FormBuilderDateTimePicker(
                    attribute: "assessmentDate",
                    inputType: InputType.date,
                    validators: [FormBuilderValidators.required(errorText: "Required")],
                    format: DateFormat("MMM dd, yyyy"),
                    autovalidate: false,
                  ),
                  SizedBox(height: 10),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      TimePicker(attribute: "startTime", label: "Start Time"),
                      SizedBox(width: 20),
                      NumericInput(
                        attribute: "duration",
                        min: 10,
                        label: "Duration (Min)",
                      ),
                    ],
                  ),
                ],
              ),
              _Island(
                children: [
                  _Required(false),
                  CheckOption(
                    attribute: "showResultOnlyOnCompletion",
                    option: FormOption(
                      label: "Show result immediately",
                      value: false,
                      description: "If you want to show results to students "
                          "immediately after they finish their assessment",
                    ),
                  ),
                  CheckOption(
                    attribute: "negativeMarks",
                    option: FormOption(
                      label: "Negative marking",
                      value: true,
                      description: "Discourage guessing by applying a penalty for every"
                          " incorrect answer",
                    ),
                  ),
                  InputLabel("Instructions"),
                  FormBuilderTextField(
                    attribute: "assessmentInstructions",
                    maxLines: 5,
                  ),
                ],
              ),
              SubmitButton(
                  route: "https://backend.geniuscorner.com/assessments/autoCreate",
                  onSuccess: (r) {
                    print(r.body);
                    Quiz quiz = Quiz.fromJson(r.body['object']);

                    if (quiz.marks == 0) return;
                    InCreate.of(context).quiz = quiz;
                    DefaultTabController.of(context).index = 1;
                  }),
            ],
          );
        },
      ),
    );
  }
}

class _Island extends StatelessWidget {
  final List<Widget> children;

  _Island({@required this.children, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 4),
      color: Colors.white,
      margin: EdgeInsets.only(bottom: 40),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: children,
      ),
    );
  }
}

class _Required extends StatelessWidget {
  final bool required; //

  _Required(this.required, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: Padding(
        padding: EdgeInsets.only(top: 5, bottom: 5),
        child: Text(
          "all ${required ? 'required' : 'optional'}".toUpperCase(),
          style: TextStyle(
              fontSize: 11,
              fontWeight: FontWeight.bold,
              color: required ? Colors.deepOrange : Colors.blue),
        ),
      ),
    );
  }
}
