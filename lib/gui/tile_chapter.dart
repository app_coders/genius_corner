import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genius_corner/create_quiz/select_skills.dart';
import 'package:genius_corner/json/chapter.dart';

class TileChapter extends StatefulWidget {
  final Chapter chapter;

  TileChapter(this.chapter, {Key key}) : super(key: key);

  _SelectState createState() => _SelectState();
}

class _SelectState extends State<TileChapter> {
  bool _checked;

  @override
  void initState() {
    super.initState();
    _checked = false;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[200],
      child: Container(
        color: Colors.white,
        margin: EdgeInsets.only(bottom: 1),
        child: CheckboxListTile(
          dense: true,
          //contentPadding: EdgeInsets.only(left: 16, top: 0, bottom: 0),
          value: _checked,
          title: Text(
            widget.chapter.name,
            style: TextStyle(fontSize: 13, fontFamily: "primary"),
          ),
          //subtitle: Text("${widget.chapter.skills.length} skill(s)"),
          controlAffinity: ListTileControlAffinity.trailing,
          onChanged: (clicked) {
            TestSkills settings = TestSkills.of(context);

            if (clicked)
              settings.addChapter(widget.chapter);
            else
              settings.removeChapter(widget.chapter);

            setState(() => _checked = clicked);
          },
        ),
      ),
    );
  }
}
