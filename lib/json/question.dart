import 'package:genius_corner/json/answer.dart';
import 'package:genius_corner/misc/server.dart';
import 'package:json_annotation/json_annotation.dart';

part 'question.g.dart';

@JsonSerializable()
class Question {
  @JsonKey(name: 'question')
  int id; //
  int marks; //
  @JsonKey(name: 'skill')
  int chapterId; //
  String text; //
  String image; //
  String explanation; //
  List<Answer> answers; //

  Question();

  factory Question.fromJson(Map<String, dynamic> json) => _$QuestionFromJson(json); //
  Map<String, dynamic> toJson() => _$QuestionToJson(this);

  String get urlDetails => "questions/$id"; //
  List<Answer> get correct => answers?.where((o) => o.isCorrect); //

  bool get hasImageOptions =>
      answers?.firstWhere((a) => a.image != null, orElse: () => null) != null;

  bool get hasLongOptions =>
      answers?.firstWhere((a) => a.text.length > 16, orElse: () => null) != null; //

  Future<void> load() async {
    Map<String, dynamic> json = await Server.get(urlDetails);

    print(json);

    this
      ..chapterId = json['skill']
      ..text = json['questionText']
      ..image = json['questionImageLoc']
      ..explanation = json['explanationLoc']
      ..answers = (json['questionResponses'] as List)
          ?.map((e) => Answer.fromJson(e as Map<String, dynamic>))
          ?.toList();
  }
}
