import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:genius_corner/create_quiz/in_create.dart';
import 'package:genius_corner/create_quiz/select_skills.dart';
import 'package:genius_corner/gui/radio_group.dart';
import 'package:genius_corner/json/section.dart';
import 'package:genius_corner/misc/form_option.dart';

class ListSections extends StatelessWidget {
  const ListSections({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    InCreate settings = InCreate.of(context);
    List<Section> sections = settings.user.sections;

    return RadioGroup(
      attribute: "subgroupId",
      options: sections.map((s) {
        return FormOption(
          label: "${s.name} \u00b7 ${s.subject.name}",
          value: s.id,
        );
      }).toList(),
      onChanged: (j) {
        Section selected = settings.user.findSection(j);

        showModalBottomSheet<Map<String, List<int>>>(
            context: context,
            isDismissible: false,
            builder: (context) {
              return SelectSkills(
                selected,
                settings.school,
              );
            }).then((value) {
          // Set values returned by BottomSheet into the form...
          if (value != null) {
            FormBuilderState form = FormBuilder.of(context);
            value.forEach((key, value) => form.setAttributeValue(key, value));
          }
        });
      },
    );
  }
}
