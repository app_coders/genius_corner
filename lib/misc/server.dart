import 'dart:convert' show jsonDecode;

import 'package:genius_corner/core/res.dart';
import 'package:http/http.dart' as http;
import 'package:uri/uri.dart';

class Server {
  static Future<Map<String, dynamic>> get(String route,
      {Map<String, dynamic> params}) async {
    String uri;

    if (params == null) {
      uri = "${Res.server}/$route";
    } else {
      String keys = params.keys.join(",");
      UriTemplate temp = UriTemplate("${Res.server}/$route{?$keys}");

      uri = temp.expand(params);
    }

    // print(uri);
    http.Response res = await http.get(uri);
    return jsonDecode(res?.body);
  }
}
