import 'package:genius_corner/json/grade.dart';
import 'package:json_annotation/json_annotation.dart';

part 'section_outline.g.dart';

@JsonSerializable()
class SectionOutline {
  int id; //
  int school; //
  @JsonKey(name: 'description')
  String name; //
  Grade grade; //

  SectionOutline();

  factory SectionOutline.fromJson(Map<String, dynamic> json) =>
      _$SectionOutlineFromJson(json); //
  Map<String, dynamic> toJson() => _$SectionOutlineToJson(this);
}
