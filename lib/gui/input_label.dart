
import 'package:flutter/cupertino.dart';
import 'package:genius_corner/core/styles.dart';

class InputLabel extends StatelessWidget {
  final String text;

  InputLabel(this.text, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 20, bottom: 4),
      child: Text(text, style: Style.label.copyWith(fontWeight: FontWeight.bold)),
    );
  }
}