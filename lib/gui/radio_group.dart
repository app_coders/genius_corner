import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:genius_corner/gui/an_option.dart';
import 'package:genius_corner/misc/form_option.dart';

class RadioGroup extends StatelessWidget {
  final String attribute; //
  final List<FormOption> options; //
  final bool shrinkWrap; //
  final Function(dynamic) onChanged; //

  RadioGroup({
    @required this.attribute,
    @required this.options,
    this.shrinkWrap = true,
    this.onChanged,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormBuilderRadioGroup(
      onChanged: onChanged,
      attribute: attribute,
      decoration: InputDecoration(
        enabledBorder: InputBorder.none,
      ),
      orientation: GroupedRadioOrientation.vertical,
      validators: [FormBuilderValidators.required(errorText: "Required")],
      materialTapTargetSize:
          shrinkWrap ? MaterialTapTargetSize.shrinkWrap : MaterialTapTargetSize.padded,
      options: options.map((e) {
        return FormBuilderFieldOption(
          value: e.value,
          child: OptionWidget(e),
        );
      }).toList(),
    );
  }
}
