import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Style {
  static const TextStyle appBarTitle = TextStyle(
    fontSize: 15,
    color: Colors.black,
    fontWeight: FontWeight.bold,
  );

  static const TextStyle label = TextStyle(fontSize: 13, fontFamily: "primary"); //
  static const TextStyle button = TextStyle(
    fontSize: 11,
    fontWeight: FontWeight.bold,
    color: Colors.blue,
  ); //

  static const TextStyle question = TextStyle(
    fontFamily: "serif",
    fontSize: 15,
    color: Colors.black,
  );

  static const TextStyle answer = TextStyle(
    fontSize: 12,
    fontFamily: "primary",
    color: Colors.black,
  );
}
