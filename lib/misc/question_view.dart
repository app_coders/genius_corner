import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genius_corner/core/styles.dart';
import 'package:genius_corner/json/answer.dart';
import 'package:genius_corner/json/question.dart';

class QuestionView extends StatefulWidget {
  final Question question;

  QuestionView(this.question, {Key key}) : super(key: key);

  _ViewState createState() => _ViewState();
}

class _ViewState extends State<QuestionView> {
  bool explanation; //

  Question get question => widget.question; //
  bool get isText => widget.question.image == null;

  @override
  Widget build(BuildContext context) {
    List<Widget> content = !explanation
        ? <Widget>[
            question.text != null
                ? Text(question.text, style: Style.question)
                : Container(),
            SizedBox(height: 10),
            question.image != null
                ? CachedNetworkImage(imageUrl: question.image)
                : Container(),
            SizedBox(height: 10),
          ]
        : <Widget>[
            Container(
              padding: EdgeInsets.all(1),
              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.circular(8),
              ),
              child: CachedNetworkImage(imageUrl: question.explanation),
            ),
            SizedBox(height: 10),
          ];

    return GestureDetector(
      onTap: () {
        if (question.explanation != null) {
          setState(() => explanation = !explanation);
        }
      },
      child: Card(
        child: Container(
          padding: EdgeInsets.all(8),
          constraints: BoxConstraints(minHeight: 80),
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    "${question.id}",
                    style: Style.label.copyWith(color: Colors.grey, fontSize: 10),
                  ),
                ),
                ...content,
                Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    "${question.marks} Marks".toUpperCase(),
                    style: TextStyle(
                      fontSize: 10,
                      fontWeight: FontWeight.bold,
                      color: Colors.blue,
                    ),
                  ),
                ),
                Divider(),
                _AnswerOptions(question),
              ],
            ),
          ),
        ),
      ),
    );
  } //

  @override
  void initState() {
    super.initState();
    explanation = false;
  }
}

class _AnswerOptions extends StatelessWidget {
  final Question question;

  _AnswerOptions(this.question, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (question.answers?.isEmpty ?? true) {
      return Text("Free-text response", style: Style.answer);
    } else {
      bool asList = question.hasImageOptions || question.hasLongOptions;
      return GridView.count(
        childAspectRatio: asList ? 15 : 6,
        crossAxisCount: asList ? 1 : 2,
        shrinkWrap: true,
        children: question.answers.map((a) => _AnswerWidget(a)).toList(),
      );
    }
  }
}

class _AnswerWidget extends StatelessWidget {
  final Answer answer;

  _AnswerWidget(this.answer, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        answer.isCorrect ? Icon(Icons.check, color: Colors.green) : Container(width: 24),
        SizedBox(width: 10),
        Container(
          constraints: BoxConstraints(maxWidth: 250),
          child: answer.text != null
              ? Text(answer.text, style: Style.answer)
              : CachedNetworkImage(imageUrl: answer.image),
        ),
      ],
    );
  }
}
