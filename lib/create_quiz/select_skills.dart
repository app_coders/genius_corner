import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genius_corner/core/res.dart';
import 'package:genius_corner/core/styles.dart';
import 'package:genius_corner/gui/tile_chapter.dart';
import 'package:genius_corner/gui/tile_skill.dart';
import 'package:genius_corner/json/chapter.dart';
import 'package:genius_corner/json/section.dart';
import 'package:genius_corner/json/skill.dart';
import 'package:http/http.dart' as http;

class SelectSkills extends StatelessWidget {
  final Section section; //
  final int school; //

  SelectSkills(this.section, this.school, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TestSkills(
      section,
      school,
      child: Builder(builder: (context) => _BeginSelection()),
    );
  }
}

class TestSkills extends InheritedWidget {
  final Section section; //
  final int school; //
  final List<Chapter> chapters; //
  final List<Skill> skills; //
  final ValueNotifier<int> numChapters; //
  final ValueNotifier<int> numSkills; //

  TestSkills(this.section, this.school, {Key key, @required Widget child})
      : chapters = List<Chapter>(),
        skills = List<Skill>(),
        numChapters = ValueNotifier<int>(0),
        numSkills = ValueNotifier<int>(0),
        super(key: key, child: child);

  static TestSkills of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType(aspect: TestSkills);
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;

  void addChapter(Chapter chapter) {
    if (chapters.contains(chapter)) return;
    chapters.add(chapter);
    numChapters.value = chapters.length;
  }

  void removeChapter(Chapter chapter) {
    if (!chapters.contains(chapter)) return;
    chapters.remove(chapter);
    numChapters.value = chapters.length;
  }

  void addSkill(Skill skill) {
    if (skills.contains(skill)) return;
    skills.add(skill);
    numSkills.value = skills.length;
  }

  void removeSkill(Skill skill) {
    if (!skills.contains(skill)) return;
    skills.remove(skill);
    numSkills.value = skills.length;
  }
}

class _BeginSelection extends StatefulWidget {
  _BeginSelection({Key key}) : super(key: key);

  _SelectState createState() => _SelectState();
}

class _SelectState extends State<_BeginSelection> {
  bool _skills;

  @override
  void initState() {
    super.initState();
    _skills = false;
  }

  @override
  Widget build(BuildContext context) {
    TestSkills settings = TestSkills.of(context);

    return Stack(
      children: [
        Container(
          padding: EdgeInsets.only(top: 70, left: 10, right: 10),
          color: Colors.white,
          child: _skills ? _ListOfSkills() : _ListOfChapters(settings.section),
        ),
        Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.grey[200],
            //borderRadius: BorderRadius.only(topLeft: corner, topRight: corner),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    _skills ? "Select Skills" : "Select Chapters",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                  ),
                  ValueListenableBuilder(
                    valueListenable: _skills ? settings.numSkills : settings.numChapters,
                    builder: (context, j, _) {
                      return Text("$j selected", style: TextStyle(fontSize: 10));
                    },
                  ),
                ],
              ),
              Spacer(),
              FlatButton(
                padding: EdgeInsets.zero,
                onPressed: () => Navigator.pop(context),
                child: Text(
                  "CANCEL",
                  style: Style.button.copyWith(color: Colors.deepOrange),
                ),
              ),
              FlatButton(
                child: Text("APPLY", style: Style.button),
                onPressed: () {
                  int numSelected =
                      _skills ? settings.numSkills.value : settings.numChapters.value;

                  if (numSelected > 0) {
                    if (!_skills) {
                      setState(() => _skills = true);
                    } else {
                      List<int> chapters = settings.chapters.map((c) => c.id).toList();
                      List<int> skills = settings.skills.map((s) => s.id).toList();

                      Navigator.pop(context, {
                        "skillIds": chapters,
                        "conceptIds": skills,
                      });
                    }
                  }
                },
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class _ListOfChapters extends StatefulWidget {
  final Section section;

  _ListOfChapters(this.section, {Key key}) : super(key: key);

  _ChapterState createState() => _ChapterState();
}

class _ChapterState extends State<_ListOfChapters> {
  Future<List<dynamic>> _loader;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _loader,
      builder: (context, AsyncSnapshot<List<dynamic>> payload) {
        if (!payload.hasData) return Container();
        List<Chapter> chapters =
            payload.data.map((e) => Chapter.fromJson(e as Map<String, dynamic>)).toList()
              ..removeWhere((chapter) => chapter.invalid)
              ..sort((a, b) => a.name.compareTo(b.name));

        return ListView(
          shrinkWrap: true,
          children: chapters.map((e) => TileChapter(e)).toList(),
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    _loader = _getChapters();
  }

  Future<List<dynamic>> _getChapters() async {
    http.Response res = await http.get("${Res.server}/${widget.section.urlChapters}");
    return jsonDecode(res?.body);
  }
}

class _ListOfSkills extends StatelessWidget {
  _ListOfSkills({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TestSkills settings = TestSkills.of(context);

    return ListView(
      shrinkWrap: true,
      children: settings.chapters.map((c) {
        return _InChapter(c, settings.school);
      }).toList(),
    );
  }
}

class _InChapter extends StatefulWidget {
  final Chapter _chapter; //
  final int school; //

  _InChapter(this._chapter, this.school, {Key key}) : super(key: key);

  _InChapterState createState() => _InChapterState();
}

class _InChapterState extends State<_InChapter> {
  Future<List<dynamic>> _loader;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _loader,
      builder: (context, AsyncSnapshot<List<dynamic>> payload) {
        if (!payload.hasData) return Container();

        List<Skill> skills = payload.data
            .map((e) => Skill.fromJson(e as Map<String, dynamic>))
            .toList()
              ..sort((a, b) => a.name.compareTo(b.name));

        if (skills.length == 0) return Container();

        return Padding(
          padding: const EdgeInsets.only(left: 5, top: 10),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget._chapter.name,
                style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
              ),
              ...skills.map((s) => TileSkill(s)).toList(),
            ],
          ),
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    _loader = _getSkills();
  }

  Future<List<dynamic>> _getSkills() async {
    String route = widget._chapter.urlSkills(widget.school);
    http.Response res = await http.get("${Res.server}/$route");

    return jsonDecode(res?.body);
  }
}
