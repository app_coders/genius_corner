import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:genius_corner/core/styles.dart';
import 'package:genius_corner/create_quiz/in_create.dart';
import 'package:genius_corner/create_quiz/tab_details.dart';
import 'package:genius_corner/create_quiz/tab_preview.dart';
import 'package:get/get.dart';

class CreateQuiz extends StatelessWidget {
  const CreateQuiz({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> payload = ModalRoute.of(context).settings.arguments;

    //print(user.sectionIds);
    return InCreate(
      payload['teacher'],
      child: Builder(builder: (context) {
        return WillPopScope(
          onWillPop: () async {
            Get.back(result: InCreate.of(context).quiz);
            return false;
          },
          child: DefaultTabController(
            length: 2,
            child: Scaffold(
              appBar: AppBar(
                  title: Text("Create Assessment", style: Style.appBarTitle),
                  bottom: _ReadOnlyTabBar(
                    child: TabBar(
                      tabs: <Widget>[
                        Tab(text: "Basic Details"),
                        Tab(text: "Preview"),
                      ],
                    ),
                  )),
              body: TabBarView(
                physics: NeverScrollableScrollPhysics(),
                children: <Widget>[
                  TabDetails(),
                  TabPreview(),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}

class _ReadOnlyTabBar extends StatelessWidget implements PreferredSizeWidget {
  final TabBar child;

  _ReadOnlyTabBar({@required this.child, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AbsorbPointer(child: child);
  }

  @override
  Size get preferredSize => this.child.preferredSize;
}

class _TimeWidget extends StatelessWidget {
  final String label; //
  final String attribute; //

  _TimeWidget({
    @required this.label,
    @required this.attribute,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label,
            style: Style.label.copyWith(
              fontSize: 11,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 5),
          FormBuilderDateTimePicker(
            attribute: attribute,
            inputType: InputType.time,
            keyboardType: TextInputType.datetime,
            validators: [FormBuilderValidators.required()],
            timePickerInitialEntryMode: TimePickerEntryMode.input,
          ),
        ],
      ),
    );
  }
}
