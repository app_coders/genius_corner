import 'package:genius_corner/json/section_outline.dart';
import 'package:genius_corner/json/subject.dart';
import 'package:json_annotation/json_annotation.dart';

part 'section.g.dart';

@JsonSerializable()
class Section {
  int id; //
  Subject subject; //
  @JsonKey(name: 'group')
  SectionOutline outline; //

  Section();

  String get name => outline.name;

  factory Section.fromJson(Map<String, dynamic> json) => _$SectionFromJson(json); //
  Map<String, dynamic> toJson() => _$SectionToJson(this);

  String get urlChapters =>
      "skills/grade/${outline.grade.id}/subject/${subject.id}/school/${outline.school}";
}
