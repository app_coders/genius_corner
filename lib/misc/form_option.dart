import 'package:flutter/foundation.dart';

class FormOption {
  String label; //
  String description; //
  dynamic value; //

  FormOption({
    @required this.label,
    @required this.value,
    this.description,
  });
}
