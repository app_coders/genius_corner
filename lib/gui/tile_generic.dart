
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TileGeneric extends StatelessWidget {
  final String title; //
  final Widget leading; //
  final Widget trailing; //
  final String subtitle; //
  final VoidCallback onTap; //

  TileGeneric({
    @required this.title,
    this.subtitle,
    this.onTap,
    IconData icon,
    Key key,
  })  : this.leading = icon != null ? Icon(icon, color: Colors.grey) : null,
        this.trailing = const Icon(Icons.chevron_right, color: Colors.blue),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    const TextStyle base = TextStyle(fontSize: 13);
    const TextStyle small = TextStyle(fontFamily: "primary", fontSize: 11, color:
    Colors.black54) ;
    double padding = leading != null ? 5 : 0 ;

    return GestureDetector(
      child: Container(
        margin: EdgeInsets.only(bottom: 1),
        padding: EdgeInsets.only(top: padding, bottom: padding),
        color: Colors.white,
        child: ListTile(
          dense: leading == null,
          title: Text(
            title,
            style: base.copyWith(fontWeight: FontWeight.bold),
          ),
          trailing: trailing,
          leading: leading,
          subtitle: Text(subtitle ?? "", style: small),
        ),
      ),
      onTap: () {
        if (onTap != null) onTap();
      }
    );
  }
}
