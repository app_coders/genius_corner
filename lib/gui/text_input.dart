import 'package:flutter/cupertino.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:genius_corner/gui/input_label.dart';

class TextInput extends StatelessWidget {
  final String attribute; //
  final String label; //
  final bool password; //

  TextInput({
    @required this.attribute,
    @required this.label,
    this.password = false,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InputLabel(label),
        FormBuilderTextField(
          attribute: attribute,
          keyboardType: TextInputType.text,
          validators: [FormBuilderValidators.required(errorText: "Can't be blank")],
          autovalidate: false,
          obscureText: password,
        ),
      ],
    );
  }
}
