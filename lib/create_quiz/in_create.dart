import 'package:flutter/cupertino.dart';
import 'package:genius_corner/json/quiz.dart';
import 'package:genius_corner/json/teacher.dart';

class InCreate extends InheritedWidget {
  final Teacher user; //
  final Map<String, dynamic> _vault; //

  InCreate(this.user, {Key key, @required Widget child})
      : _vault = Map<String, dynamic>(),
        super(key: key, child: child);

  static InCreate of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType(aspect: InCreate);
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;

  int get userId => user.id; //
  int get school => user.school; //
  String get sectionIds => user.sectionIds; //

  Quiz get quiz => _vault['quiz']; //
  set quiz(Quiz q) => _vault['quiz'] = q; //
}
