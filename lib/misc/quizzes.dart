import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genius_corner/core/routes.dart';
import 'package:genius_corner/core/styles.dart';
import 'package:genius_corner/gui/tile_generic.dart';
import 'package:genius_corner/json/quiz.dart';
import 'package:genius_corner/json/section.dart';
import 'package:genius_corner/json/teacher.dart';
import 'package:get/get.dart';

class Quizzes extends StatefulWidget {
  final Teacher teacher; //
  final String type; //

  Quizzes(this.teacher, {@required this.type, Key key}) : super(key: key);

  _ListState createState() => _ListState();
}

class _ListState extends State<Quizzes> {
  int section; //
  bool showFilter; //
  List<Quiz> masterList; //
  List<Quiz> filteredList; //

  Teacher get teacher => widget.teacher; //
  List<Quiz> get list => filteredList ?? masterList; //

  @override
  void initState() {
    super.initState();

    String type = widget.type.toLowerCase();

    showFilter = type != "live";

    if (type == "live")
      masterList = teacher.live;
    else if (type == "saved")
      masterList = teacher.saved;
    else
      masterList = teacher.finished;

    if (showFilter) {
      section = teacher.sections.first.id;
      filteredList = masterList
        ..where((q) => q.sectionId == section)
        ..sort((a, b) => b.id.compareTo(a.id));
    }
  }

  @override
  Widget build(BuildContext context) {
    if (!showFilter) return _RenderList(teacher, list);

    return ListView(
      shrinkWrap: true,
      children: [
        Container(
          constraints: BoxConstraints(minHeight: 110),
          decoration: BoxDecoration(color: Colors.white),
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Show assessments for",
                  style: Style.label.copyWith(fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 5),
                Container(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey[300], width: 1),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: DropdownButton(
                    value: section,
                    underline: Container(),
                    style: Style.label.copyWith(color: Colors.black),
                    items: teacher.sections.map((s) {
                      return DropdownMenuItem(
                        value: s.id,
                        child: Text("${s.name} \u00b7 ${s.subject.name}"),
                      );
                    }).toList(),
                    onChanged: (j) {
                      setState(() {
                        section = j;
                        filteredList = masterList.where((q) => q.sectionId == j).toList();
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
        _ResultCount(filteredList.length),
        _RenderList(teacher, list),
      ],
    );
  }
}

class _RenderList extends StatelessWidget {
  final Teacher _teacher; //
  final List<Quiz> _list; //

  _RenderList(this._teacher, this._list, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      children: _list.map((q) {
        Section section = _teacher.findSection(q.sectionId);

        return TileGeneric(
          title: q.name,
          subtitle: "${section.name} \u00b7 ${section.subject.name}"
              " \u00b7 ${q.numQuestions} Questions",
          onTap: () => Get.toNamed(
            Screen.loadQuiz,
            arguments: {'quiz': q, 'teacher': _teacher},
          ),
        );
      }).toList(),
    );
  }
}

class _ResultCount extends StatelessWidget {
  final int count;

  _ResultCount(this.count, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 8, bottom: 8),
      constraints: BoxConstraints(minHeight: 60),
      child: Align(
        alignment: Alignment.bottomLeft,
        child: Text("$count Assessment(s)",
            style: Style.label.copyWith(fontWeight: FontWeight.bold)),
      ),
    );
  }
}
