import 'package:genius_corner/json/chapter.dart';
import 'package:json_annotation/json_annotation.dart';

part 'quiz.g.dart';

@JsonSerializable()
class Quiz {
  int id; //
  String status; //
  @JsonKey(name: 'description')
  String name; //
  @JsonKey(name: 'totalQuestions')
  int numQuestions; //
  @JsonKey(name: 'totalMarks')
  int marks; //
  @JsonKey(name: 'subGroup')
  int sectionId; //
  @JsonKey(name: 'totalHours')
  int duration; //
  @JsonKey(name: 'skills')
  String chapterIds;

  List<Chapter> chapters; //
  @JsonKey(name: 'assessmentDate')
  DateTime date; //

  Quiz();

  factory Quiz.fromJson(Map<String, dynamic> json) => _$QuizFromJson(json); //
  Map<String, dynamic> toJson() => _$QuizToJson(this); //

  bool get saved => status == "Created"; //
  bool get finished => status == "Complete"; //
  bool get live => status == "Activated"; //
}
