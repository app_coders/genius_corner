import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genius_corner/core/styles.dart';
import 'package:genius_corner/misc/form_option.dart';

class OptionWidget extends StatelessWidget {
  final FormOption option; //

  OptionWidget(this.option, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool hasDesc = option.description != null;

    return Container(
      width: 240,
      padding: EdgeInsets.only(left: 10, right: 5, top: 15, bottom: 15),
      margin: EdgeInsets.only(top: 5),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey[300], width: 1),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: hasDesc
            ? [
          Text(option.label,
              style: Style.label.copyWith(fontWeight: FontWeight.bold)),
          Text(option.description, style: Style.label.copyWith(color: Colors.grey))
        ]
            : [
          Text(option.label, style: Style.label),
        ],
      ),
    );
  }
}