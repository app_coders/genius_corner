import 'package:genius_corner/json/quiz.dart';
import 'package:genius_corner/json/section.dart';
import 'package:genius_corner/misc/server.dart';
import 'package:json_annotation/json_annotation.dart';

part 'teacher.g.dart';

@JsonSerializable()
class Teacher {
  int id; //
  int school; //
  String name; //
  @JsonKey(name: 'pic_url')
  String picUrl; //
  List<Quiz> quizzes; //
  @JsonKey(name: 'subgroups')
  String sectionIds; //
  List<Section> sections; //

  Teacher();

  List<Quiz> get live => quizzes.where((q) => q.live).toList(); //
  List<Quiz> get saved => quizzes.where((q) => q.saved).toList(); //
  List<Quiz> get finished => quizzes.where((q) => q.finished).toList(); //

  Section findSection(int id) {
    return sections?.firstWhere((s) => s.id == id, orElse: () => null);
  }

  factory Teacher.fromJson(Map<String, dynamic> json) => _$TeacherFromJson(json); //
  Map<String, dynamic> toJson() => _$TeacherToJson(this);

  Future<List<Section>> getSections() async {
    if (sections?.isNotEmpty ?? false) return sections;

    sections = await Future.wait(sectionIds.split(",").map((j) {
      return _details(j);
    }).toList());
    return sections;
  }

  Future<Section> _details(String section) async {
    Map<String, dynamic> json = await Server.get("subgroups/$section");
    return Section.fromJson(json);
  }

  Future<void> getQuizzes() async {
    if (quizzes?.isNotEmpty ?? false) return;
    quizzes = List<Quiz>();

    List<List<Quiz>> bySection = await Future.wait(sectionIds.split(",").map((j) {
      return _assignedTo(j);
    }).toList());

    bySection.forEach((subList) => quizzes.addAll(subList));
    // print(list.length);
  }

  Future<List<Quiz>> _assignedTo(String j) async {
    Map<String, dynamic> json =
        await Server.get("teachers/findAssessments/$id?subGroupId=$j&status=all");

    List<Quiz> sectionQuizzes = (json['object'] as List).map((e) {
      Map<String, dynamic> data = (e as Map<String, dynamic>)['assessment'];
      return Quiz.fromJson(data);
    }).toList();

    return sectionQuizzes;
  }
}
