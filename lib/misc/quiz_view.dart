import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:genius_corner/core/res.dart';
import 'package:genius_corner/core/styles.dart';
import 'package:genius_corner/json/chapter.dart';
import 'package:genius_corner/json/question.dart';
import 'package:genius_corner/json/quiz.dart';
import 'package:genius_corner/json/section.dart';
import 'package:genius_corner/json/teacher.dart';
import 'package:genius_corner/misc/question_view.dart';
import 'package:genius_corner/misc/server.dart';
import 'package:http/http.dart' as http;

class QuizView extends StatefulWidget {
  final Teacher teacher; //
  final Quiz quiz; //

  QuizView(this.quiz, {@required this.teacher, Key key}) : super(key: key);

  _PreviewState createState() => _PreviewState();
}

class _PreviewState extends State<QuizView> {
  Future<List<Question>> _loader;

  Quiz get quiz => widget.quiz; //
  Teacher get teacher => widget.teacher; //

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _loader,
      builder: (context, AsyncSnapshot<List<Question>> payload) {
        if (!payload.hasData)
          return Center(
            child: SpinKitCircle(color: Colors.grey),
          );

        List<Question> questions = payload.data;
        List<int> covered = questions.map((q) => q.chapterId).toList();

        quiz.chapters.removeWhere((c) => !covered.contains(c.id));
        // quiz.chapters.forEach((c) => print(c.name));

        return Padding(
          padding: const EdgeInsets.only(left: 5, right: 5),
          child: ListView(
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            children: <Widget>[
              ...quiz.chapters.map((c) {
                return _ByChapter(
                    c, questions.where((q) => q.chapterId == c.id).toList());
              }).toList(),
            ],
          ),
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    _loader = _fuckMeHard();
  }

  Future<List<Question>> _fuckMeHard() async {
    Section section = widget.teacher.findSection(widget.quiz.sectionId);

    if (widget.quiz.chapters?.isEmpty ?? true) {
      // chapters = f(section = g(quiz))
      // Note: Not all chapters may be covered in this quiz. So, we will also
      // group questions by chapters - all the work the backend should've been doing.
      http.Response res = await http.get("${Res.server}/${section.urlChapters}");
      List<dynamic> list = jsonDecode(res?.body);

      widget.quiz.chapters = list
          .map((e) => Chapter.fromJson(e as Map<String, dynamic>))
          .toList()
            ..sort((a, b) => a.name.compareTo(b.name));

      // widget.quiz.chapters.forEach((c) => print(c.name));
    }

    Map<String, dynamic> json = await Server.get("assessments/${widget.quiz.id}");
    List<Question> questions = (json['assessmentQuestions'] as List)
        .map((e) => Question.fromJson(e as Map<String, dynamic>))
        .toList()
          ..sort((a, b) => a.marks.compareTo(b.marks));

    await Future.wait(questions.map((q) => q.load()).toList());
    return questions;
  }
}

class _ByChapter extends StatelessWidget {
  final Chapter _chapter; //
  final List<Question> _questions; //

  _ByChapter(this._chapter, this._questions, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int numQuestions = _questions.length;
    int marks = 0;

    _questions.forEach((q) => marks += q.marks);

    return Container(
      padding: EdgeInsets.all(8),
      margin: EdgeInsets.only(top: 2),
      decoration: BoxDecoration(
        //color: Colors.white,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(_chapter.name, style: Style.appBarTitle),
          Text(
            "$numQuestions Questions \u00b7 $marks Marks",
            style: TextStyle(fontSize: 12, color: Colors.black),
          ),
          SizedBox(height: 20),
          ..._questions.map((q) => QuestionView(q)).toList(),
        ],
      ),
    );
  }
}
