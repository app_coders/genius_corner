import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genius_corner/core/styles.dart';
import 'package:genius_corner/json/quiz.dart';
import 'package:genius_corner/json/teacher.dart';
import 'package:genius_corner/misc/quiz_view.dart';

class LoadQuiz extends StatelessWidget {
  const LoadQuiz({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> args = ModalRoute.of(context).settings.arguments;
    Quiz quiz = args['quiz'];
    Teacher teacher = args['teacher'];

    return Scaffold(
      appBar: AppBar(
        title: Text(quiz.name, style: Style.appBarTitle),
      ),
      body: QuizView(quiz, teacher: teacher),
    );
  }
}
