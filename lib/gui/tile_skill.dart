import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genius_corner/create_quiz/select_skills.dart';
import 'package:genius_corner/json/skill.dart';

class TileSkill extends StatefulWidget {
  final Skill skill;

  TileSkill(this.skill, {Key key}) : super(key: key);

  _SelectState createState() => _SelectState();
}

class _SelectState extends State<TileSkill> {
  bool _checked;

  @override
  void initState() {
    super.initState();
    _checked = false;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[200],
      child: Container(
        color: Colors.white,
        margin: EdgeInsets.only(bottom: 1),
        child: CheckboxListTile(
          dense: true,
          contentPadding: EdgeInsets.zero,
          value: _checked,
          title: Text(
            widget.skill.name,
            style: TextStyle(fontSize: 13, fontFamily: "primary"),
          ),
          //subtitle: Text("${widget.skill.skills.length} skill(s)"),
          controlAffinity: ListTileControlAffinity.trailing,
          onChanged: (clicked) {
            TestSkills settings = TestSkills.of(context);

            if (clicked)
              settings.addSkill(widget.skill);
            else
              settings.removeSkill(widget.skill);

            setState(() => _checked = clicked);
          },
        ),
      ),
    );
  }
}
