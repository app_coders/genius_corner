import 'package:json_annotation/json_annotation.dart';

part 'answer.g.dart';

@JsonSerializable()
class Answer {
  int id; //
  @JsonKey(defaultValue: false)
  bool isCorrect; //
  @JsonKey(name: 'responseText')
  String text; //
  @JsonKey(name: 'imageNameLoc')
  String image; //

  Answer();

  factory Answer.fromJson(Map<String, dynamic> json) => _$AnswerFromJson(json); //
  Map<String, dynamic> toJson() => _$AnswerToJson(this);
}
