import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SVGImage extends StatelessWidget {
  final String file; //
  final double width; //
  final double height; //
  final Color color; //

  const SVGImage(this.file, {this.width, this.height, this.color});

  @override
  Widget build(BuildContext context) {
    try {
      // String fileName = file.contains("/") ? "$file.svg" : "core/$file.svg" ;
      String fileName = "$file.svg";

      return SvgPicture.asset(
        "assets/svg/$fileName",
        width: width,
        height: height,
        color: color,
      );
    } catch (_) {
      return Container(width: width, height: height);
    }
  }
}
