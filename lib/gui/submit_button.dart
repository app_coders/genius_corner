import 'dart:convert' show jsonDecode, jsonEncode;

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart'
    show FormBuilder, FormBuilderState;
import 'package:http/http.dart' as http;
import 'package:progress_indicator_button/progress_button.dart';

class SubmitButton extends StatefulWidget {
  final String route; //
  final Function(Reply) onSuccess; //
  final String label; //

  SubmitButton({
    @required this.route,
    @required this.onSuccess,
    this.label,
    Key key,
  }) : super(key: key);

  _SubmitState createState() => _SubmitState();
}

class _SubmitState extends State<SubmitButton> {
  String error;

  @override
  Widget build(BuildContext context) {
    FormBuilderState form = FormBuilder.of(context);

    return ProgressButton(
      child: _Label(widget.label, error),
      color: Colors.red,
      progressIndicatorColor: Colors.white,
      progressIndicatorSize: 20,
      //borderRadius: BorderRadius.circular(8),
      strokeWidth: 3,
      onPressed: (AnimationController spin) {
        if (spin.isAnimating) return;
        bool validated = form?.saveAndValidate() ?? false;

        spin.forward();
        if (validated) {
          _submitPOST(form.value).then((reply) {
            spin.reverse();
            if (reply.success) {
              widget.onSuccess(reply);
            } else {
              setState(() {
                String message = reply.badRequest ? reply.body['error'] : 'network error';
                error = message;
              });
            }
          });
        } else {
          spin.reverse();
          return;
        }
      },
    );
  }

  /*
  Future<void> _attach(http.MultipartRequest post, String path,
      {@required String name}) async {
    if (path == null || path.isEmpty) return;

    File pic = File(path);
    List<int> bytes = await pic.readAsBytes();

    http.MultipartFile attach = http.MultipartFile.fromBytes(
      'attachment',
      bytes,
      filename: name,
      contentType: MediaType.parse(
        lookupMimeType(pic.path),
      ),
    );

    post.files.add(attach);
  }

   */

  Future<Reply> _submitPOST(Map<String, dynamic> form) async {
    // jsonEncode cannot encode DateTime. So, any such field must first be
    // be converted to String.
    form.forEach((key, value) {
      if (value is DateTime) {
        form[key] = value.toIso8601String();
      }
    });

    String body = jsonEncode(form);

    // print("===== BEFORE SUBMITTING");
    // print(body);
    try {
      http.Response response = await http.post(
        widget.route,
        body: body,
        headers: {"Content-Type": "application/json"},
      );

      Map<String, dynamic> json = jsonDecode(response?.body);
      return Reply(status: response.statusCode, body: json);
    } catch (e) {
      return Reply(status: 400);
    }
  }
}

class _Label extends StatelessWidget {
  final String label; //
  final String error; //

  _Label(this.label, this.error, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const TextStyle style = TextStyle(fontSize: 16, color: Colors.white);
    if (error == null) return Text(label ?? "SUBMIT", style: style);

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text("RE-SUBMIT", style: style),
        Text(error, style: style.copyWith(fontSize: 10, color: Colors.white))
      ],
    );
  }
}

class Reply {
  int status; // HTTP status code
  Map<String, dynamic> body;

  Reply({@required this.status, this.body});

  bool get success => status == 200; //
  bool get badRequest => status == 400; //
  bool get serverError => status == 500; //
}
