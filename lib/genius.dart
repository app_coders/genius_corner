import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genius_corner/core/routes.dart';
import 'package:genius_corner/screens/console.dart';
import 'package:genius_corner/screens/create_quiz.dart';
import 'package:genius_corner/screens/launch.dart';
import 'package:genius_corner/screens/list_quizzes.dart';
import 'package:genius_corner/screens/load_quiz.dart';
import 'package:genius_corner/screens/sign_in.dart';
import 'package:get/get.dart';

class Genius extends StatelessWidget {
  const Genius({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        fontFamily: 'primary',
        scaffoldBackgroundColor: Colors.blueGrey[50],
        tabBarTheme: TabBarTheme(
          labelColor: Colors.black,
          labelStyle: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.bold,
            fontFamily: "primary",
          ),
          unselectedLabelColor: Colors.grey,
          unselectedLabelStyle: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.normal,
            fontFamily: "primary",
          ),
        ),
        inputDecorationTheme: InputDecorationTheme(
            isDense: true,
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey[300]),
            ),
            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.blue)),
            errorBorder:
                OutlineInputBorder(borderSide: BorderSide(color: Colors.deepOrange))),
      ),
      navigatorKey: Get.key,
      initialRoute: Screen.launch,
      routes: {
        Screen.signIn: (context) => SignIn(),
        Screen.console: (context) => Console(),
        Screen.createQuiz: (context) => const CreateQuiz(),
        Screen.loadQuiz: (context) => const LoadQuiz(),
        Screen.listQuizzes: (context) => ListQuizzes(),
        Screen.launch: (context) => const Launch(),
      },
    );
  }
}
