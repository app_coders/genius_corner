import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:genius_corner/core/styles.dart';

class TimePicker extends StatelessWidget {
  final String label; //
  final String attribute; //

  TimePicker({
    @required this.attribute,
    @required this.label,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label, style: Style.label.copyWith(fontWeight: FontWeight.bold)),
          SizedBox(height: 5),
          FormBuilderDateTimePicker(
            attribute: attribute,
            inputType: InputType.time,
            timePickerInitialEntryMode: TimePickerEntryMode.dial,
            validators: [FormBuilderValidators.required(errorText: "Required")],
            autovalidate: false,
          ),
        ],
      ),
    );
  }
}
