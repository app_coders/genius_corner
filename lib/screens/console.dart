import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:genius_corner/core/kees.dart';
import 'package:genius_corner/core/routes.dart';
import 'package:genius_corner/core/styles.dart';
import 'package:genius_corner/gui/svg_image.dart';
import 'package:genius_corner/gui/tile_generic.dart';
import 'package:genius_corner/json/quiz.dart';
import 'package:genius_corner/json/teacher.dart';
import 'package:genius_corner/misc/quizzes.dart';
import 'package:genius_corner/misc/server.dart';
import 'package:genius_corner/screens/in_console.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Console extends StatefulWidget {
  Console({Key key}) : super(key: key);

  _ConsoleState createState() => _ConsoleState();
}

class _ConsoleState extends State<Console> {
  Future<Teacher> loader;

  @override
  void initState() {
    super.initState();
    loader = _getTeacher();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getTeacher(),
      builder: (context, AsyncSnapshot<Teacher> payload) {
        if (!payload.hasData) {
          return Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SVGImage("gc", height: 120),
                SizedBox(height: 20),
                SpinKitCircle(color: Colors.grey, size: 50),
              ],
            ),
          );
        }

        return InConsole(
          payload.data,
          child: Builder(
            builder: (context) {
              InConsole settings = InConsole.of(context);
              String picUrl = settings.user?.picUrl;

              return Scaffold(
                appBar: AppBar(
                  title: SVGImage("gc-2", height: 35),
                  actions: <Widget>[
                    FlatButton(
                      child: Text("Need help?", style: Style.button),
                      onPressed: () => print(" Give help"),
                    ),
                    FlatButton.icon(
                      onPressed: () {
                        SharedPreferences.getInstance().then((prefs) {
                          prefs
                              .setInt(Kee.userId, 0)
                              .then((_) => Get.offAndToNamed(Screen.signIn));
                        });
                      },
                      icon: Icon(Icons.exit_to_app, color: Colors.blue, size: 24),
                      label: Text("Logout", style: Style.button),
                    ),
                  ],
                ),
                body: ValueListenableBuilder(
                    valueListenable: settings.live,
                    builder: (context, numLive, _) {
                      Teacher teacher = settings.user;

                      return ListView(
                        shrinkWrap: true,
                        children: <Widget>[
                          _Header(title: "Welcome, ${settings.user?.name}"),
                          TileGeneric(
                            title: "Create Assessments",
                            icon: Icons.create_new_folder,
                            subtitle:
                                "Assessments are a great way to see what your students know",
                            onTap: () => Get.toNamed(
                              Screen.createQuiz,
                              arguments: {'teacher': settings.user},
                            ).then((ret) {
                              Quiz q = ret as Quiz;
                              // print("${q?.name ?? 'Nothing'} created ....");
                              if (q != null) {
                                settings.user.quizzes.add(q);
                                settings.saved.value += 1;
                              }
                            }),
                          ),
                          ValueListenableBuilder(
                            valueListenable: settings.saved,
                            builder: (context, j, _) {
                              return TileGeneric(
                                title: "Saved Assessments ($j)",
                                icon: Icons.save,
                                subtitle:
                                    "Assessments saved for modifying or publishing later",
                                onTap: () {
                                  Get.toNamed(Screen.listQuizzes, arguments: {
                                    'teacher': teacher,
                                    'type': 'Saved',
                                  });
                                },
                              );
                            },
                          ),
                          ValueListenableBuilder(
                            valueListenable: settings.finished,
                            builder: (context, j, _) {
                              return TileGeneric(
                                title: "Finished Assessments ($j)",
                                icon: Icons.folder_special,
                                subtitle:
                                    "Evaluate and publish results of finished assessments",
                                onTap: () {
                                  Get.toNamed(Screen.listQuizzes, arguments: {
                                    'teacher': teacher,
                                    'type': 'Finished',
                                  });
                                },
                              );
                            },
                          ),
                          _Header(title: "Live Assessments ($numLive)"),
                          Quizzes(teacher, type: "Live"),
                        ],
                      );
                    }),
              );
            },
          ),
        );
      },
    );
  }

  Future<Teacher> _getTeacher() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int id = prefs.getInt(Kee.userId) ?? 58277;

    print(id);
    Map<String, dynamic> json = await Server.get("teachers/$id");
    print(json);
    Teacher teacher = Teacher.fromJson(json);

    await Future.wait([
      teacher.getQuizzes(),
      teacher.getSections(),
    ]);
    return teacher;
  }
}

class _Header extends StatelessWidget {
  final String title; //
  final Widget child; //

  _Header({this.title, this.child, Key key})
      : assert((title == null) ^ (child == null)),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10, top: 20, bottom: 10),
      child: child != null
          ? child
          : Text(
              title,
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
                fontFamily: "secondary",
              ),
            ),
    );
  }
}
