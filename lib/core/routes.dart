class Screen {
  static const String console = "/console"; //
  static const String createQuiz = "/create_quiz"; //
  static const String editProfile = "/edit_profile"; //
  static const String launch = "/launch"; //
  static const String listQuizzes = "/list_quizzes"; //
  static const String loadQuiz = "/load_quiz"; //
  static const String signIn = "/sign_in"; //
}
