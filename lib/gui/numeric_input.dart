import 'package:flutter/cupertino.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:genius_corner/gui/input_label.dart';

class NumericInput extends StatelessWidget {
  final int min; //
  final String attribute; //
  final String label; //

  NumericInput({
    @required this.min,
    @required this.attribute,
    @required this.label,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InputLabel(label),
          FormBuilderTextField(
            attribute: attribute,
            keyboardType: TextInputType.number,
            validators: [
              FormBuilderValidators.min(min, errorText: "Must be >= $min"),
              FormBuilderValidators.required(errorText: "Required"),
            ],
            autovalidate: false,
          ),
        ],
      ),
    );
  }
}
