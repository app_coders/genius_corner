import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genius_corner/core/kees.dart';
import 'package:genius_corner/screens/console.dart';
import 'package:genius_corner/screens/sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Launch extends StatelessWidget {
  const Launch({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: SharedPreferences.getInstance(),
        builder: (context, AsyncSnapshot<SharedPreferences> payload) {
          if (!payload.hasData) return Container();

          int id = payload.data.getInt(Kee.userId) ?? 0;
          if (id > 0)
            return Console();
          else
            return SignIn();
        },
      ),
    );
  }
}
