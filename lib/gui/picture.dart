
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genius_corner/gui/svg_image.dart';

class Picture extends StatelessWidget {
  final String url ; //
  final double radius ; //

  const Picture({this.url, this.radius = 20, Key key}) : super(key: key) ;

  @override
  Widget build(BuildContext context) {
    if (url == null) {
      return SVGImage('profile-pic', width: 2 * radius);
    } else {
      return CircleAvatar(
        radius: radius,
        backgroundColor: Colors.transparent,
        backgroundImage: CachedNetworkImageProvider(url),
      );
    }
  }
}