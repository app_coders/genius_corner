import 'package:flutter/cupertino.dart';
import 'package:genius_corner/json/teacher.dart';

class InConsole extends InheritedWidget {
  final Teacher user; //
  final ValueNotifier<int> saved; //
  final ValueNotifier<int> finished; //
  final ValueNotifier<int> live; //

  InConsole(Teacher teacher, {@required Widget child, Key key})
      : user = teacher,
        saved = ValueNotifier<int>(teacher.saved?.length ?? 0),
        finished = ValueNotifier<int>(teacher.finished?.length ?? 0),
        live = ValueNotifier<int>(teacher.live?.length ?? 0),
        super(child: child, key: key);

  static InConsole of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType(aspect: InConsole);
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}
