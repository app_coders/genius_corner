import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genius_corner/core/styles.dart';
import 'package:genius_corner/create_quiz/in_create.dart';
import 'package:genius_corner/json/quiz.dart';
import 'package:genius_corner/misc/quiz_view.dart';

class TabPreview extends StatelessWidget {
  TabPreview({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    InCreate settings = InCreate.of(context);

    return ListView(
      shrinkWrap: true,
      children: [
        _Congrats(settings.quiz),
        SizedBox(height: 30),
        QuizView(settings.quiz, teacher: settings.user),
      ],
    );
  }
}

class _Congrats extends StatelessWidget {
  final Quiz quiz;

  _Congrats(this.quiz, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextStyle style = TextStyle(
      fontSize: 11,
      color: Colors.grey,
      fontFamily: "primary",
    );
    return Container(
      padding: EdgeInsets.all(8),
      constraints: BoxConstraints(minHeight: 100),
      color: Colors.white,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(Icons.thumb_up, color: Colors.grey, size: 32),
          SizedBox(width: 20),
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Congratulations!", style: Style.appBarTitle),
              Text(
                "Assessment ready. Please review the questions",
                style: style,
              ),
              Text("${quiz.numQuestions} Questions \u00b7 ${quiz.marks} Marks",
                  style: style),
            ],
          ),
        ],
      ),
    );
  }
}
