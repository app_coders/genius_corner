import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:genius_corner/core/kees.dart';
import 'package:genius_corner/core/res.dart';
import 'package:genius_corner/core/routes.dart';
import 'package:genius_corner/gui/submit_button.dart';
import 'package:genius_corner/gui/svg_image.dart';
import 'package:genius_corner/gui/text_input.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignIn extends StatelessWidget {
  SignIn({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          constraints: BoxConstraints(maxWidth: 250),
          child: FormBuilder(
            initialValue: {"loginType": "Teacher"},
            child: Builder(
              builder: (context) {
                return ListView(
                  shrinkWrap: true,
                  children: [
                    SVGImage("gc", width: 150),
                    SizedBox(height: 20),
                    TextInput(attribute: "loginName", label: "Username"),
                    TextInput(attribute: "password", label: "Password", password: true),
                    SizedBox(height: 20),
                    SubmitButton(
                      route: "${Res.server}/validateLogin",
                      onSuccess: (r) {
                        int id = int.parse(r.body['id']);
                        SharedPreferences.getInstance().then((prefs) {
                          prefs
                              .setInt(Kee.userId, id)
                              .then((_) => Get.offAndToNamed(Screen.console));
                        });
                      },
                      label: "Login",
                    ),
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
