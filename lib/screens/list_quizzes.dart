import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genius_corner/core/styles.dart';
import 'package:genius_corner/json/teacher.dart';
import 'package:genius_corner/misc/quizzes.dart';

class ListQuizzes extends StatelessWidget {
  ListQuizzes({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> args = ModalRoute.of(context).settings.arguments;
    Teacher teacher = args['teacher'];
    String type = args['type'];

    return Scaffold(
      appBar: AppBar(
        title: Text("$type Assessments", style: Style.appBarTitle),
      ),
      body: Quizzes(teacher, type: type),
    );
  }
}
