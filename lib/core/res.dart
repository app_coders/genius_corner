class Res {
  static bool get _debugMode {
    bool debug = false;
    assert(debug = true); // asserts aren't executed in debug mode.
    return debug;
  }

  static String server = _debugMode
      ? "https://uat-backend.geniuscorner.com"
      : "https://backend.geniuscorner.com";
}
