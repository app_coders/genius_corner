import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:genius_corner/core/styles.dart';
import 'package:genius_corner/gui/an_option.dart';
import 'package:genius_corner/misc/form_option.dart';


class CheckOption extends StatelessWidget {
  final String attribute; //
  final FormOption option ; //
  final bool shrinkWrap; //

  CheckOption({
    @required this.attribute,
    @required this.option,
    this.shrinkWrap = true,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormBuilderCheckbox(
      initialValue: false,
      attribute: attribute,
      leadingInput: true,
      decoration: InputDecoration(
        border: InputBorder.none,
        enabledBorder: InputBorder.none,
      ),
      materialTapTargetSize:
      shrinkWrap ? MaterialTapTargetSize.shrinkWrap : MaterialTapTargetSize.padded,
      label:  OptionWidget(option),
    );
  }
}


